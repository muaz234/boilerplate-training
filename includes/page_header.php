
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN PAGE TITLE-->
                        <div class="m-subheader m--hidden-mobile">
    						<div class="d-flex align-items-center">
    							<div class="mr-auto">
    								<h3 class="m-subheader__title display-hide-mobile">
    									 <?php //echo $lang['header']; ?><small class="text-muted"></small>
    								</h3>
    								<h3 class="m-subheader__title display-show-mobile display-hide-desktop display-hide-tablet">
    									 <?php //echo $lang['header_resp']; ?><small class="text-muted"></small>
    								</h3>
    							</div>
                                <div>
                                    <span class="custom-ajax-activity display-hide-mobile hidden"><i class="fa fa-refresh fa-spin text-default"></i>&nbsp;</span>
                                    <span class="custom-title-notification text-muted display-hide-mobile display-hide-tablet tooltips" data-toggle="tooltip" data-placement="bottom" title="<?php //echo $lang['tt_laststatus'];?>"><span class="custom-title-notification-text"><?php // echo $_SESSION['user_name']." ".$_SESSION['user_timestamp'];?></span></span>
                                </div>
                                <div>
                                    <a href="#" class="custom-speedicon-init btn btn-sm btn-default display-hide-mobile tooltips" data-toggle="tooltip" data-placement="bottom" title="<?php // echo $lang['tt_netcheck']; ?>"><span><i class="fa fa-wifi faa-flash animated"></i></span></a>
                                    <a href="#" class="custom-speedicon custom-speedicon-fast btn btn-sm blue display-hide-mobile tooltips" data-toggle="tooltip" data-placement="bottom" title="<?php // echo $lang['tt_netspeed']; ?>"><span><i class="fa fa-wifi faa-flash animated"></i> <span><?php // echo $lang['label_fast']; ?></span></span></a>
                                    <a href="#" class="custom-speedicon custom-speedicon-slow btn yellow-casablanca btn-sm display-hide-mobile tooltips " data-toggle="tooltip" data-placement="bottom" title="<?php // echo $lang['tt_netspeed']; ?>"><span><i class="fa fa-wifi faa-flash animated"></i> <span class=""><?php // echo $lang['label_slow']; ?></span></span></a>
                                    <a href="#" class="custom-speedicon custom-speedicon-normal btn blue-dark btn-sm display-hide-mobile tooltips " data-toggle="tooltip" data-placement="bottom" title="<?php // echo $lang['tt_netspeed']; ?>"><span><i class="fa fa-wifi faa-flash animated"></i> <span><?php // echo $lang['label_normal']; ?></span></span></a>
                                    <a href="#" class="custom-speedicon custom-speedicon-none btn red-mint btn-sm display-hide-mobile tooltips " data-toggle="tooltip" data-placement="bottom" title="<?php // echo $lang['tt_netspeed']; ?>"><span><i class="fa fa-wifi faa-flash animated"></i> <span><?php // echo $lang['label_none']; ?></span></span></a>
                                </div>
    						</div>
    					</div>
                        <!-- END PAGE TITLE-->
                        <!-- BEING PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb display-hide-mobile text-muted">
                                <li>
                                    <span class="faa-parent animated-hover tooltips" data-toggle="tooltip" data-placement="bottom" title="<?php // echo $lang['tt_lastupdate']; ?>"><i class="fa fa-database faa-ring"></i> <span><?php //echo $lang['label_lastupdate']; ?></span>  <?php // echo date("d M Y, g:ia"); ?> <i class="custom-ajax-activity fa fa-refresh fa-spin display-hide-mobile hidden"></i></span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div class="pull-right tooltips btn btn-sm">
                                    <?php
                                        if(isset($page_header_extension)){
                                            echo $page_header_extension;
                                        }
                                    ?>
                                    <img class="custom-enterprise-search-logo" src="<?php // echo $data_layout['path_searchicon']; ?>"/>
<!--                                    <input id="autocomplete_globalsearchbox" class="custom-enterprise-search autocomplete-input" placeholder="--><?php //// echo $lang['input_globalsearch']; ?><!-- (--><?php ////echo $_SESSION['autocomplete_records'];?><!--)" type="text" data-calltype="main"/>-->
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- END PAGE HEADER-->