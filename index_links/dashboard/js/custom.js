$(document).ready(function () {
    $('#calendar1').fullCalendar({
        timeFormat: 'hh:mm a',
        events: [
            {
                "allDay": "true",
                "title": "Hari Buruh",
                "id": "1",
                "end": "2018-05-01 23:59:00",
                "start": "2018-05-01 00:00:00",
                "color": "#3598DC"
            },
            {
                "allDay": "true",
                "title": "Pilihanraya",
                "id": "2",
                "end": "2018-05-09 23:59:00",
                "start": "2018-05-09 00:00:00",
                "color": "#3598DC"
            },
            {
                "allDay": "true",
                "title": "Pegawai Sulh 1",
                "id": "3",
                "end": "2018-05-18 5:00:00",
                "start": "2018-05-14 9:00:00",
                "color": "#E7505A"
            },
            {
                "allDay": "true",
                "title": "Hakim 1",
                "id": "3",
                "end": "2018-05-18 12:00:00",
                "start": "2018-05-16 9:00:00",
                "color": "#32C5D2"
            }
        ]
    });


});