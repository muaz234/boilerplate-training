var amchartsObjects = {
	options : {
        chart_generator_template: {
            "chart_replace": {
                "chart_attr": {
                    "divId": "[[portlet_id]]_body",
                    "titles": [{"text": ""}, {"text": ""}],
                    "changeChart": true,
                    "nolegend":false,
                    "graphs":[{}],
                    "valueAxes":[{},{}],
                    "categoryAxis":{},
                    "interval":0,
                    "generator": true,
                    "legend":{
                        "enabled":false
                    }
                }
            }
        },
        sentiment_chart_clone: {
            "id": "sentiment_chart_clone",
            "type": "serial",
            "dataProvider": [],
            "categoryField": "date",
            "autoMargins": false,
            "marginBottom": 30,
            "marginLeft": 30,
            "balloon": {
                "textAlign": "left"
            },  
            "colors":["#777777","#777777"],              
            "graphs": [{
                "title": "Bar",
                "lineColor": "#000000",
                "lineAlpha": 0.8,
                "bulletSize":5,
                "bulletAlpha":0.5,
                "bullet":"bubble",
                "type": "column",
                "fillAlphas": 0.8,
                "valueField": "value",
                "fillColorsField":"color",
                "balloonText": "<b>[[mediaTitle]]</b><br>Date: [[dateText]]<br>Sentiment: [[value]] - [[sentiment]]",
            },
            {
                "title": "Line",
                "lineAlpha": 0.8,
                "lineColorField":"color",
                "lineThickness": 3,
                "bulletSize":10,
                "bulletAlpha":0.8,
                "bullet":"diamond",
                "type": "line",
                "fillAlphas": 0,
                "valueField": "value",
                "balloonText": "<b>[[mediaTitle]]</b><br>Date: [[dateText]]<br>Sentiment: [[value]] - [[sentiment]]",
            }            
            ],
            "valueAxes": [{
                "baseValue":0.5,
                "minimum": 0,
                "maximum": 1
            }],
            "categoryAxis": {
                "parseDates": true,
                "labelsEnabled": false,
                "equalSpacing": true
            },
			"chartCursor": {
				"color": "#EBECFF",
				"categoryBalloonAlpha": 1,
	            "cursorAlpha": 0.3,
	            "cursorColor":"#000000",
	            "fullWidth":false,
                "zoomable": true,
                "valueBalloonsEnabled":false
            },
            "legend":{
                "useGraphSettings": true,
                "align": "center",
                "autoMargins": false,
                "marginBottom": 20
	        }                                   
        },      
        sentiment_breakdown_clone: {
	        "id": "sentiment_chart_clone",
	        "type": "pie",
	        "titles": [{text:"NEWS SENTIMENT BREAKDOWN"}],
	       	"startDuration": globalChartStartDuration,
	        "startEffect": globalChartStartEffect,
	        "sequencedAnimation": globalChartSequencedAnimation,          
	        "prefixesOfBigNumbers":globalChartPrefixesOfBigNumbers,                                  		              
            "usePrefixes":globalChartUsePrefixes,
            "angle": globalChartAnglePie,
            "depth3D": 10,
            "alpha": 0.8,
            "colors":[],
            "dataProvider": [],
            "fontSize": globalChartFontSize, 
	        "innerRadius": "50%",
	        "balloonText": "[[title]]<br><span><b>[[value]] ([[percents]]%)</b></span><br>",
	        "titleField": "category",
	        "valueField": "value",
	        "allLabels": [],
	        "labelText": "[[label]] ([[value]])<br>[[percents]]%",           
	        "balloon": {},
	        "valueAxes": [{}],            
	        "export": {
	            "enabled": true,
                "menuReviver": function(item,li) {
                    if (item.format === "PRINT" || item.label == "Save as ...")
                        li.style.display = "none";
                    return li;
                }
            },
            "hideLabelsPercent":5
        },
        news_exposure_clone: {
            "id": "news_exposure_clone",
            "type": "serial",
            "titles": [{text:"NEWS EXPOSURE"}],
            "dataProvider": [],
            "categoryField": "date",
            "autoMargins": false,
            "marginBottom": 50,
            "marginLeft": 30,
            "balloon": {
                "textAlign": "left"
            },  
            "colors":["#005fa0"],              
            "graphs": [{
                "title": "Bar",
                "lineAlpha": 0.8,
                "bullet":"none",
                "type": "column",
                "fillAlphas": 0.8,
                "valueField": "value",
                "balloonText": "<b>Date:</b> [[dateView]]<br><b>Total Exposure:</b> [[value]]",
            }],
            "valueAxes": [{
                "minimum": 0,
                "integersOnly": true
            }],
            "categoryAxis": {
                "parseDates": true,
                "labelsEnabled": false,
                "equalSpacing": true
            },
			"chartCursor": {
				"color": "#EBECFF",
				"categoryBalloonAlpha": 1,
	            "cursorAlpha": 0.3,
	            "cursorColor":"#000000",
	            "fullWidth":false,
                "zoomable": true,
                "valueBalloonsEnabled":false
            }
        },                  
        radar_chart_clone: {
	    	"id": "radar_chart_clone",
	    	"type": "radar",
	        "titles": globalChartTitles,
	        "color": globalChartColor,
	        "startDuration": globalChartStartDuration,
	        "startEffect": globalChartStartEffect,
	        "sequencedAnimation": globalChartSequencedAnimation,
	        "prefixesOfBigNumbers":globalChartPrefixesOfBigNumbers,                        
	        "usePrefixes":globalChartUsePrefixes,        
	        "autoDisplay": globalChartAutoDisplay,    
	        "dataDateFormat": globalDataDateFormat,
	        "categoryField": "category",
	        "plotAreaFillColors": globalChartPlotAreaFillColors,
	        "plotAreaFillAlphas": globalChartPlotAreaFillAlphas, //3D            			
	        "backgroundColor": globalChartBackgroundColor,
	        "backgroundAlpha": globalChartBackgroundAlpha,
	        "borderColor": globalChartBorderColor,
	        "borderAlpha": globalChartBorderAlpha,        
            "fontSize": globalChartFontSize, 
	        "guides": [],
	        "allLabels": [],
            "colors":[],
	        "export": {
	        	"enabled": true,
                "menuReviver": function(item,li) {
                    if (item.format === "PRINT" || item.label == "Save as ...")
                        li.style.display = "none";
                    return li;
                }
            },
			"categoryAxis": {},                                    
	        "valueAxes": [
                {
                    "id":"v1",
                    "position":"left",
                    "gridAlpha":0.3,
                    "gridColor":"#000000",
                    "axisAlpha":0.3,
                    "axisColor":"#000000"      
                },
                {
                    "id":"v2",
                    "position":"right",
                    "gridAlpha":0.3,
                    "gridColor":"#000000",
                    "axisAlpha":0.3,
                    "axisColor":"#000000"      
                }
            ],
            "legend":{
				"align": "center",
                "enabled": false,
                "backgroundAlpha": 0,
                "backgroundColor": "#CCCCCC",
                "borderAlpha": 0,
                "borderColor": "#AAAAAA",
                "equalWidths": false,
                "useGraphSettings": true,
                "valueText":""
	        },
	        "graphs": [{
                "balloonText":"<b>[[label]]</b><br>[[value]]",
                "fillAlphas":0.5,
                "lineAlpha":1,
                "bullet":"round",
                "lineThickness":3,
                "bulletSize":10,
                "valueField":"value"
            }],
	        "dataProvider": []
        },                                              	            
	   
        bar_chart_clone: {
	    	"id": "bar_chart_clone",
	        "type": "serial",
            "addClassNames": true,
	        "titles": globalChartTitles,
	        "prefixesOfBigNumbers":globalChartPrefixesOfBigNumbers,                        
	        "usePrefixes":globalChartUsePrefixes,
	        "dataDateFormat": globalDataDateFormat,
	        "startDuration": globalChartStartDuration,
	        "startEffect": globalChartStartEffect,
	        "autoDisplay": globalChartAutoDisplay,
	        "sequencedAnimation": globalChartSequencedAnimation,
	        "plotAreaFillColors": globalChartPlotAreaFillColors,
	        "plotAreaFillAlphas": globalChartPlotAreaFillAlphas, //3D            			
	        "backgroundColor": globalChartBackgroundColor,
	        "backgroundAlpha": globalChartBackgroundAlpha,
	        "borderColor": globalChartBorderColor,
	        "borderAlpha": globalChartBorderAlpha,
            "color": globalChartColor,
	        "depth3D": globalChartDepth3DBar,
	        "angle": globalChartAngleBar,            
            "fontSize": globalChartFontSize, 
            "trendLines": [],
            "mouseWheelZoomEnabled": globalMouseWheelChartZoom,
            "graphs":[{
                "valueAxis": "v1",
                "balloonText":"<b>[[category]]</b><br>[[value]]",
                "id":"AmGraph-0",
                "fillAlphas":0.8,
                "lineAlpha":0.9,
                "type":"column",
                "valueField":"value",
                "labelOffset":10,      
                "color":"#FFFFFF",
                "topRadius":1,
                "labelText": "",
                "labelPosition": "middle"
            }],      
	        "dataProvider": [],
	        "categoryField": "category",            
	        "guides": [],
            "valueAxes":[
                {  
                    "id":"v1",
                    "position": "left",      			
                    "autoGridCount":false,
                    "axisAlpha":1,
                    "title":""
                },
                {  
                    "id":"v2",
                    "position": "right",      			
                    "autoGridCount":false,
                    "axisAlpha":1,
                    "title":""
                }            
            ],                                                      
	        "allLabels": [],
	        "balloon": {},
	        "legend":{
				"align": "center",
                "enabled": false,
                "backgroundAlpha": 0,
                "backgroundColor": "#CCCCCC",
                "borderAlpha": 0,
                "borderColor": "#AAAAAA",
                "equalWidths": false,
                "useGraphSettings": true,
                "valueText":""
	        },
			"categoryAxis": {
				"gridPosition": "start",
                "gridAlpha": 0.2,
				"axisAlpha": 0.2,
                "axisColor": "#cccccc",
                "gridColor": "#cccccc",
				"tickLength": 0,
				"parseDates": false,
        		"minHorizontalGap": 1,
        		"minVerticalGap": 1,  
                "autoWrap": true,              
                "labelFunction": function(label) {
                    if (label && label.length > globalChartSubstr) {
                        return label.substr(0, globalChartSubstr) + '...';
                    }                        
                    return label;
                }                                                                
			},                        
			"chartCursor": {
				"color": "#EBECFF",
				"categoryBalloonAlpha": 1,
	            "cursorAlpha": 0.3,
	            "cursorColor":"#000000",
	            "fullWidth":false,
                "zoomable": true,
                "valueBalloonsEnabled":false
			}, 
	        "export": {
                "enabled": true,
                "menuReviver": function(item,li) {
                    if (item.format === "PRINT" || item.label == "Save as ...")
                        li.style.display = "none";
                    return li;
                }                
            },
            "defs": {
                "filter": {
                  "id": "dropshadow",
                  "x": "0%",
                  "y": "0%",
                  "width": "130%",
                  "height": "130%",
                  "feOffset": {
                    "result": "offOut",
                    "in": "SourceAlpha",
                    "dx": "15",
                    "dy": "15"
                  },
                  "feGaussianBlur": {
                    "result": "blurOut",
                    "in": "offOut",
                    "stdDeviation": "12"
                  },
                  "feBlend": {
                    "in": "SourceGraphic",
                    "in2": "blurOut",
                    "mode": "normal"
                  }
                }
            }
        },
        	   
        pie_chart_clone: {
	        "id": "pie_chart_clone",
	        "type": "pie",
	        "titles": globalChartTitles,
	        "color": globalChartColor,
	        "backgroundColor": globalChartBackgroundColor,
	        "backgroundAlpha": globalChartBackgroundAlpha,  
	        "borderColor": globalChartBorderColor,
	        "borderAlpha": globalChartBorderAlpha,                      
	       	"startDuration": globalChartStartDuration,
	        "startEffect": globalChartStartEffect,
	        "sequencedAnimation": globalChartSequencedAnimation,          
	        "autoDisplay": globalChartAutoDisplay,              
	        "prefixesOfBigNumbers":globalChartPrefixesOfBigNumbers,                                  		              
	        "usePrefixes":globalChartUsePrefixes,
	        "dataDateFormat": globalDataDateFormat,
            "labelTickColor": globalPieTickColor,
            "colors":[],
            "dataProvider": [],
	        "alpha": 0.7,
            "fontSize": globalChartFontSize, 
	        "innerRadius": "50%",
	        "depth3D": globalChartDepth3DPie,
	        "angle": globalChartAnglePie,
	        "balloonText": "[[title]]<br><span><b>[[value]] ([[percents]]%)</b></span><br>",
	        "outlineAlpha": 0.22,
	        "outlineColor": globalPieOutlineColor,
	        "titleField": "category",
	        "valueField": "value",
	        "allLabels": [],
	        "labelText": "[[label]]<br>[[percents]]%",           
	        "balloon": {},
	        "legend":{
                "enabled": false,
				        "align": "center",
                "backgroundAlpha": 0,
                "backgroundColor": "#CCCCCC",
                "borderAlpha": 0,
                "borderColor": "#AAAAAA",
                "equalWidths": false,
                "valueText":""
	        },            
	        "valueAxes": [{}],            
	        "export": {
	            "enabled": true,
                "menuReviver": function(item,li) {
                    if (item.format === "PRINT" || item.label == "Save as ...")
                        li.style.display = "none";
                    return li;
                }
            },
            "hideLabelsPercent":5
        },               
	},


    bar_chart_clone: function(objParam){  
        //console.log(objParam);
        globalChartConfigs[objParam.divId] = objParam;

        $("#"+objParam.divId).parent().find(".custom-chart-artefact").fadeOut(500);
        globalLoadingMessage(objParam.divId);
        var chartType = objParam.chartType;

        if(!objParam.changeChart){ //FIRST LOAD            
            if($("#"+objParam.divId).attr("data-graph-type") != ""){
                if(!globalModalFullscreen){
                    chartType = $("#"+objParam.divId).attr("data-graph-type");
                }
                else{
                    chartType = $("#"+objParam.sourceId).attr("data-graph-type");
                }    
            }
            else{
                var params = {
                    method: "get_save_portletgraphtype",
                    calltype: "portlet",
                    portletId: objParam.id,
                    portletGraphType: chartType
                };

                $.get("backend/ajax_call.php?"+$.param(params))
                .done(function( data ) {
                });    
            }

            if(objParam.comboChart){
                $("#"+objParam.id+" .custom-chart-select").addClass("hidden");
            }                                    
        }

        $.post( "backend/ajax_call.php", objParam.querystring)
        .done(function( data ) {     
            if(data){
                try{
                    data =  JSON.parse(data);
                    if(data){       
                        data.map(function(v,k,e){    
                            if(v.overall_total){
        
                                if(objParam.eventType != "refresh"){
                                    var filterTextDiv = $("#"+objParam.id).find(".custom-filtertext-container");
                                    var filterText = filterTextDiv.html();
                                    filterText += "<div class='custom-filtertext-label'><label class='label label-info label-sm'> "+$("#chart_total").val()+":</label> "+v.overall_total+" "+$("#chart_record").val()+"</div>";                                                        
                                    filterTextDiv.html(filterText);
                                }
                            
                                var graphType = chartType;
        
                                $("#"+objParam.divId).attr("data-graph-type",graphType);
        
                                if(!globalModalFullscreen){
                                    $("#"+objParam.id+" .custom-chart-select").addClass("hidden");
                                }
                
                                //OVERWRITE IF SETTINGS COME FROM BACKEND
                                objParam.dataProvider = v.dataProvider;
        
                                if (typeof v.graphs !== 'undefined' && v.graphs.length > 0) {
                                    objParam.graphs = v.graphs;
                                }                        
                                if (typeof v.valueAxes !== 'undefined' && v.valueAxes.length > 0) {
                                    objParam.valueAxes = v.valueAxes;
                                }                        
                                //OVERWRITE IF SETTINGS COME FROM BACKEND
        
                                if(objParam.graphs.length == 1){
                                    objParam.graphs[0].fillColorsField = "color";
                                    objParam.graphs[0].lineColorField = "color";
                                }
        
                                //ADDITIONAL GRAPH PROPERTIES BASED ON TYPE

                                //START THIS IS THE AREA FOR COMBO CHARTS
                                var comboChart = false;
                                if(objParam.comboChart){
                                    comboChart = true;
                                }
                                //END THIS IS THE AREA FOR COMBO CHARTS    

                                if(graphType == "column"){
                                    chartType = "column";
                                    
                                    objParam.graphs.map(function(x){
                                        x.lineAlpha = 0.3;  
                                        x.fillAlphas = 0.9;
                                        //x.color = "#FFFFFF";  
                                        //x.labelPosition = "middle";
        
                                        if(objParam.generator){
                                            x.bullet = "none";
                                        }

                                        if(!comboChart){
                                            x.type = chartType;
                                        }
                                        else{
                                            if(x.type == "line"){
                                                x.fillAlphas = 0;
                                                x.bullet = "round";
                                                x.lineAlpha = 1;
                                            }
                                        }
                                    });
        
                                    if(!objParam.comboChart){
                                        $("#"+objParam.id+" .custom-chart-select-area").removeClass("hidden");                                    
                                    }
                                }
                                else if(graphType == "area" || graphType == "line"){
                                    chartType = "line";
                                    if(graphType == "area"){
                                        objParam.graphs.map(function(x,index){
                                            x.lineAlpha = 0.7;
                                            x.fillAlphas = 0.5;
                                            x.lineThickness = 4;
                                            if(objParam.colorSelect){ //FROM GENERATOR COLOR
                                                x.lineColor = objParam.colorSelect;
                                                x.fillColors = objParam.colorSelect;
                                            }                                    
                                            else if(objParam.colors){
                                                x.lineColor = objParam.colors[index];
                                                x.fillColors = objParam.colors[index];
                                            }
                                            else{
                                                x.fillColors = globalRandomColor();
                                            }
                                            
                                            if(comboChart){
                                                if(x.type == "line"){
                                                    x.fillAlphas = 0;
                                                    x.bullet = "round";
                                                    x.lineAlpha = 1;
                                                }                                                    
                                            }
                                        });
        
                                        if(!objParam.comboChart){
                                            $("#"+objParam.id+" .custom-chart-select-line").removeClass("hidden"); 
                                        }
                                    }
                                    else if(graphType == "line"){
                                        objParam.graphs.map(function(x,index){
                                            x.lineAlpha = 1;
                                            x.fillAlphas = 0;
                                            x.lineThickness = 4;
                                        
                                            if(objParam.colorSelect){ //FROM GENERATOR COLOR
                                                x.lineColor = objParam.colorSelect;
                                            }                                
                                            else if(objParam.colors){
                                                x.lineColor = objParam.colors[index];
                                            }
                                            else{
                                                x.lineColor = globalRandomColor();
                                            }
                                            if(comboChart){
                                                if(x.type == "column"){
                                                    x.fillAlphas = 0.9;
                                                    x.bullet = "none";
                                                    x.lineThickness = 1;
                                                }                                                    
                                            }
                                        });
        
                                        if(!objParam.comboChart){
                                            $("#"+objParam.id+" .custom-chart-select-bar").removeClass("hidden");
                                        }
                                    }                                
        
                                    objParam.graphs.map(function(x){
                                        x.bulletSize = 10;
                                        x.bulletAlpha = 0.5;
                                        x.bullet = "bubble";
                                        x.color = "#000000";  
                                        x.labelPosition = "top";

                                        if(!comboChart){
                                            x.type = chartType;
                                        }
                                        else{
                                            if(x.type == "column"){
                                                x.fillAlphas = 0.9;
                                                x.bullet = "none";
                                                x.lineThickness = 1;
                                            }                                                    
                                        }
                                    });
                                }       
        
                                //ADDITIONAL GRAPH PROPERTIES BASED ON TYPE
        
                                objParam.dataProvider.map(function(x,index){
                                    if (x.label && x.label.length > globalChartSubstr) {
                                        x.label = x.label.substr(0, globalChartSubstr) + '...';
                                    } 

                                    if(graphType == "column"){
                                        if(objParam.colorSelect){ //FROM GENERATOR COLOR
                                            x.color =  globalSingleColor(objParam.colorSelect);
                                        }
                                        else if(objParam.colors){ //FROM GENERATOR COLOR
                                            x.color = objParam.colors[index];
                                        }
                                        else{
                                            x.color = globalRandomColor();
                                        }
                                    }
                                    if(!x.labelColorField){
                                        x.labelColorField = x.color;
                                    }
                                });
        
                                if(globalModalFullscreen && !globalDeviceMobile){
                                    objParam.export.enabled = true;
                                    objParam.marginLeft = objParam.marginLeft + 10; 
                                    objParam.fontSize = objParam.fontSize * 1.3;
        
                                    if(!objParam.generator){
                                        objParam.graphs.map(function(x){
                                            if(x.topRadius){
                                                objParam.depth3D = objParam.depth3D*2;
                                                objParam.angle = objParam.angle*2;
                                            }
                                        });    
                                    }
                                    
                                    var modal_title = $("#"+globalModalFullScreenAttr.modalId+" .custom-modal-title-text").html();
                                    var modal_total = "";
                                    if(v.overall_total){
                                        modal_total = " [ TOTAL: "+chartFunctions.nFormatter(v.overall_total)+" ]";
                                    }
                                    $("#"+globalModalFullScreenAttr.modalId+" .custom-modal-title-text").html(modal_title);
                                    $("#"+globalModalFullScreenAttr.modalId+" .custom-modal-title-text-total").html(modal_total);
                                       
                                    //LABEL OFFSET IF AVAILABLE
                                    objParam.graphs.map(function(x){
                                        if(x.labelOffset){
                                            x.labelOffset = x.labelOffset * 2;
                                        }
                                    });
                                }
        
                                if(globalDeviceMobile){
                                    objParam.graphs.map(function(x){
                                        x.labelText = "";
                                    });
        
                                    objParam.legend.enabled = false;
                                    objParam.titles[0].text = "";
                                    objParam.titles[1].text = "";
                                    objParam.marginTop = 10;
                                }
        
                                //ADDITIONAL DATA PROVIDER & GRAPH PROPERTIES HERE
                                for(var k = 0; k < objParam.dataProvider.length; k++) { 
                                }        
                                
                                objParam.graphs.map(function(x){
                                    //x.fontSize = objParam.fontSize+2;
                                    x.showHandOnHover = true;
        
                                    //ONLY SHOW LEGEND WHERE THE GRAPH IS VISIBLE
                                    x.visibleInLegend = false;
                                    for (var key in v.enabledGraphsTitle) {
                                        if(x.title == v.enabledGraphsTitle[key]){
                                            x.visibleInLegend = true;
                                            break;
                                        }
                                    }
                                });
        
                                //SET CHART PROPERTIES
                                if(objParam.generator){
                                    if(objParam.graphs.length > 1){
                                        objParam.graphs.map(function(x,index){
                                            if(x.fillColorsField){
                                                delete x.fillColorsField;
                                                delete x.lineColorField;
                                            }
                                            if(objParam.colorSelect){
                                                x.fillColors = objParam.colorSelect;
                                                x.lineColor = objParam.colorSelect;
                                            }
                                            else if(objParam.colors){
                                                x.fillColors = objParam.colors[index];
                                                x.lineColor = objParam.colors[index];
                                            }
                                            else{
                                                x.fillColors = globalRandomColor();
                                            }
                                        });            
                                    }
                                }    
        
                                //ADDITIONAL DATA PROVIDER & GRAPH PROPERTIES HERE
                                var chart = AmCharts.makeChart(objParam.divId, objParam);

                                //INTERVALS
                                if(objParam.interval && parseInt(objParam.interval) > 0){
                                    var index = globalIntervalIds.indexOf(objParam.id);
                                    if (index > -1) {
                                        globalIntervalIds.splice(index, 1);
                                        clearInterval(globalIntervalTimers[objParam.id]);
                                    }                                        
                                    globalIntervalGenerator("chart",objParam.id,objParam.interval,objParam.streamData,objParam.streamDataCatLimit);
                                }
                                
                                chart.animateAgain();

                                if(!objParam.streamData){
                                    chart.addListener("clickGraphItem", function (event) {
                                        var category = (event.item.category);
                                        if(objParam.chartclick){
                                            if(objParam.chartclick.method){
                                                objParam.chartclick.extraquery_link_id[0].target_value = category;
                                                    
                                                if(!globalModalFullscreen){
                                                    if($("#"+objParam.divId).attr("data-tablefilter")){
                                                        objParam.chartclick.tablefilter = $("#"+objParam.divId).attr("data-tablefilter");
                                                    }
                                                }
                                                else{
                                                    if($("#"+objParam.sourceId).attr("data-tablefilter")){
                                                        objParam.chartclick.tablefilter = $("#"+objParam.sourceId).attr("data-tablefilter");
                                                    }
                                                }
    
                                                if(objParam.querystring.graphCount > 1){
                                                    objParam.chartclick.stackvalue = event.graph.valueField;
                                                }           
                                                globalLoadModal(objParam.chartclick);    
                                            }
                                        }
                                    });                         
                                }
                            }
                            else{
                                globalNoDataMessage(objParam.divId);
                            }                       		        
                        });
                    }                    
                    globalExpandCollapsePortlet(objParam.id);
                }
                catch(e){
                    globalFailedMessage(objParam.divId);
                    console.log(e);
                }
            }
        });      		              
    },

    pie_chart_clone: function(objParam){  
        globalChartConfigs[objParam.divId] = objParam;
        
        $("#"+objParam.divId).parent().find(".custom-chart-artefact").fadeOut(500);
        globalLoadingMessage(objParam.divId);
        var chartType = objParam.chartType;

        if(!objParam.changeChart){ //FIRST LOAD 
            if($("#"+objParam.divId).attr("data-graph-type") != ""){
                if(!globalModalFullscreen){
                    chartType = $("#"+objParam.divId).attr("data-graph-type");
                }
                else{
                    chartType = $("#"+objParam.sourceId).attr("data-graph-type");
                }   
            }
            else{
                var params = {
                    method: "get_save_portletgraphtype",
                    calltype: "portlet",
                    portletId: objParam.id,
                    portletGraphType: chartType
                };

                $.get("backend/ajax_call.php?"+$.param(params))
                .done(function( data ) {
                });    
            }
        }

        $.post( "backend/ajax_call.php", objParam.querystring)
        .done(function( data ) {     
            if(data){
                try{
                    data =  JSON.parse(data);
                    if(data){                    
                        data.map(function(v,k,e){                    
                            if(v.overall_total){
        
                                if(objParam.eventType != "refresh"){
                                    var filterTextDiv = $("#"+objParam.id).find(".custom-filtertext-container");
                                    var filterText = filterTextDiv.html();
                                    filterText += "<div class='custom-filtertext-label'><label class='label label-info label-sm'> "+$("#chart_total").val()+":</label> "+v.overall_total+" "+$("#chart_record").val()+"</div>";
                                    filterTextDiv.html(filterText);
                                }
                                
                                var graphType = chartType;
                                $("#"+objParam.divId).attr("data-graph-type",graphType);
        
                                if(!globalModalFullscreen){
                                    $("#"+objParam.id+" .custom-chart-select").addClass("hidden");
                                }
                
                                //BLOCK CHART CONFIG HERE
                                objParam.dataProvider = v.dataProvider;
                                
                                if(!objParam.generator){
                                    if(graphType == "pie"){
                                        objParam.innerRadius = "0%";
                                        $("#"+objParam.id+" .custom-chart-select-donut").removeClass("hidden");
                                    }
                                    else if(graphType == "donut"){
                                        objParam.innerRadius = "40%";
                                        $("#"+objParam.id+" .custom-chart-select-pie").removeClass("hidden");
                                    }                          
        
                                }
                                if(!objParam.innerRadius){
                                    objParam.innerRadius = "0%";
                                }

                                if (objParam.labelColorField === undefined){
                                    //objParam.labelColorField = "labelColorField";
                                }                                                                                            
                                                        
                                for(var i = 0; i < objParam.dataProvider.length; i++) { 
                                    if(objParam.colorSelect){ //FROM GENERATOR COLOR
                                        objParam.colors.push(globalSingleColor(objParam.colorSelect));
                                        objParam.dataProvider[i]['labelColorField'] =  objParam.colors[i];
                                    }
                                    else if(!objParam.dataProvider[i].nocustomcolor){
                                        objParam.colors.push(globalRandomColor());
                                        if(!objParam.dataProvider[i]['labelColorField']){
                                            objParam.dataProvider[i]['labelColorField'] =  objParam.colors[i];
                                        }
                                    }
                                    if (objParam.dataProvider[i].label && objParam.dataProvider[i].label.length > globalChartSubstr) {
                                        objParam.dataProvider[i].label = objParam.dataProvider[i].category.substr(0, globalChartSubstr) + '...';
                                    }
                                }
        
                                //OVERWRITE IF TITLE/SUBTITLE EXISTS IN DATAPROVIDER
                                if (objParam.dataProvider[0].title) {
                                    objParam.titles[0].text = objParam.titles[0].text+objParam.dataProvider[0].title;
                                }                        
                                if (objParam.dataProvider[0].subtitle) {
                                    objParam.titles[1].text = objParam.titles[1].text+objParam.dataProvider[0].subtitle;
                                }       
        
                                if(globalModalFullscreen && !globalDeviceMobile){
        
                                    if(objParam.generator){
                                        objParam.nolegend = false;
                                        objParam.labelRadius = 35;
                                    }  
        
                                    objParam.export.enabled = true;
                                    objParam.hideLabelsPercent = 3;
                                    objParam.fontSize = objParam.fontSize * 1.3;
                                    objParam.depth3D = objParam.depth3D*2;
                                    objParam.pullOutRadius = "10%";   
                                    objParam.pulledField = "pulled";
                                    objParam.alpha = 0.7;
        
                                    var modal_title = $("#"+globalModalFullScreenAttr.modalId+" .custom-modal-title-text").html();
                                    var modal_total = "";
                                    if(v.overall_total){
                                        modal_total = " [ TOTAL: "+chartFunctions.nFormatter(v.overall_total)+" ]";
                                    }
                                    $("#"+globalModalFullScreenAttr.modalId+" .custom-modal-title-text").html(modal_title);
                                    $("#"+globalModalFullScreenAttr.modalId+" .custom-modal-title-text-total").html(modal_total);
                                    
                                
                                    if(objParam.labelRadius < 0){
                                        objParam.labelRadius =  (objParam.labelRadius - (objParam.fontSize*2.5));
                                    }
                                    else{
                                        objParam.labelRadius =  (objParam.labelRadius + (objParam.fontSize*1.5));
                                    }
                                }  
                                
                                if(globalDeviceMobile){
                                    objParam.labelRadius = 7;
                                    objParam.titles[0].text = "";
                                    objParam.titles[1].text = "";
                                    objParam.marginTop = 10;
                                }
                                
                                if(objParam.strictLabelsPercent){
                                    objParam.hideLabelsPercent = 0;
                                }
                                if(objParam.strictInnerRadius){
                                    objParam.innerRadius = objParam.strictInnerRadius;
                                }
                                if(objParam.fontSizeIncrease){
                                    objParam.fontSize = objParam.fontSize+objParam.fontSizeIncrease;
                                }
        
                                //END RESPONSIVE PIE CHART
                                /*
                                if(globalThemeBrightness == "light"){
                                    objParam.gradientRatio = [-0.5, -0.2, -0.3];
                                }
                                else if(globalThemeBrightness == "dark"){
                                    objParam.gradientRatio = [0.2, 0, -0.5];                            
                                }
                                */
        
                                var chart = AmCharts.makeChart(objParam.divId, objParam);

                                //INTERVALS
                                if(objParam.interval && parseInt(objParam.interval) > 0){
                                    var index = globalIntervalIds.indexOf(objParam.id);
                                    if (index > -1) {
                                        globalIntervalIds.splice(index, 1);
                                        clearInterval(globalIntervalTimers[objParam.id]);
                                    }                                        
                                    globalIntervalGenerator("chart",objParam.id,objParam.interval);
                               }
        
                                chart.animateAgain();
                                   
                                   chart.addListener("clickSlice", function (event) {
                                    var category = (event.dataItem.dataContext.category);
                                    if(objParam.chartclick){
                                        if(objParam.chartclick.method){
                                            objParam.chartclick.extraquery_link_id[0].target_value = category;
        
                                            if(!globalModalFullscreen){
                                                if($("#"+objParam.divId).attr("data-tablefilter")){
                                                    objParam.chartclick.tablefilter = $("#"+objParam.divId).attr("data-tablefilter");
                                                }
                                            }
                                            else{
                                                if($("#"+objParam.sourceId).attr("data-tablefilter")){
                                                    objParam.chartclick.tablefilter = $("#"+objParam.sourceId).attr("data-tablefilter");
                                                }
                                            }
        
                                            globalLoadModal(objParam.chartclick);
                                        }
                                    }        
                                }); 
                            }
                            else{
                                globalNoDataMessage(objParam.divId);
                            }                       		        
                        });
                    }

                    globalExpandCollapsePortlet(objParam.id);
                }
                catch(e){
                    globalFailedMessage(objParam.divId);
                    console.log(e);
                }
            }
        });      		              
    },

    radar_chart_clone: function(objParam){  

        globalChartConfigs[objParam.divId] = objParam;
        
        $("#"+objParam.divId).parent().find(".custom-chart-artefact").fadeOut(500);
        globalLoadingMessage(objParam.divId);
        var chartType = objParam.chartType;

        if(!objParam.changeChart){ //FIRST LOAD 
            if($("#"+objParam.divId).attr("data-graph-type") != ""){
                if(!globalModalFullscreen){
                    chartType = $("#"+objParam.divId).attr("data-graph-type");
                }
                else{
                    chartType = $("#"+objParam.sourceId).attr("data-graph-type");
                }   
            }
            else{
                var params = {
                    method: "get_save_portletgraphtype",
                    calltype: "portlet",
                    portletId: objParam.id,
                    portletGraphType: chartType
                };

                $.get("backend/ajax_call.php?"+$.param(params))
                .done(function( data ) {
                });    
            }
        }        

        $.post( "backend/ajax_call.php", objParam.querystring)
        .done(function( data ) {     
            if(data){
                try{
                    data =  JSON.parse(data);
                    if(data){                    
                        data.map(function(v,k,e){                    
                            if(v.overall_total){
        
                                if(objParam.eventType != "refresh"){
                                    var filterTextDiv = $("#"+objParam.id).find(".custom-filtertext-container");
                                    var filterText = filterTextDiv.html();
                                    filterText += "<div class='custom-filtertext-label'><label class='label label-info label-sm'> "+$("#chart_total").val()+":</label> "+v.overall_total+" "+$("#chart_record").val()+"</div>";
                                    filterTextDiv.html(filterText);
                                }
                                var graphType = chartType;
                                $("#"+objParam.divId).attr("data-graph-type",graphType);
                            
                                $("#"+objParam.id+" .custom-chart-select").addClass("hidden");
        
                                //BLOCK CHART CONFIG HERE
                                objParam.dataProvider = v.dataProvider;
                                
                                //OVERWRITE IF SETTINGS COME FROM BACKEND
                                if (typeof v.graphs !== 'undefined' && v.graphs.length > 0) {
                                    objParam.graphs = v.graphs;
                                }                        
                                if (typeof v.valueAxes !== 'undefined' && v.valueAxes.length > 0) {
                                    objParam.valueAxes = v.valueAxes;
                                }                        
                                //OVERWRITE IF SETTINGS COME FROM BACKEND
                                
                                objParam.colors.push(globalRandomColor());
        
                                if(objParam.colorSelect){ //FROM GENERATOR COLOR
                                    objParam.graphs.map(function(x){
                                        x.lineColor = objParam.colorSelect;
                                    });
                                }                                
        
                                for(var i = 0; i < objParam.dataProvider.length; i++) { 
                                    if (objParam.dataProvider[i].category && objParam.dataProvider[i].category.length > globalChartSubstr) {
                                        if(!globalModalFullscreen){
                                            objParam.dataProvider[i].category = objParam.dataProvider[i].category.substr(0, globalChartSubstr) + '...';
                                        }
                                    }                        
                                }
                                
                                if(globalModalFullscreen && !globalDeviceMobile){
                                    objParam.export.enabled = true;
                                    objParam.fontSize = objParam.fontSize * 1.3;
                                    var modal_title = $("#"+globalModalFullScreenAttr.modalId+" .custom-modal-title-text").html();
                                    var modal_total = "";
                                    if(v.overall_total){
                                        modal_total = " [ TOTAL: "+chartFunctions.nFormatter(v.overall_total)+" ]";
                                    }
                                    $("#"+globalModalFullScreenAttr.modalId+" .custom-modal-title-text").html(modal_title);
                                    $("#"+globalModalFullScreenAttr.modalId+" .custom-modal-title-text-total").html(modal_total);
                                }
        
                                if(globalDeviceMobile){
                                    objParam.titles[0].text = "";
                                    objParam.titles[1].text = "";
                                    objParam.marginTop = 10;
                                }
        
                                //SET CHART PROPERTIES
                                if(objParam.generator){
                                    if(objParam.graphs.length > 1){
                                        objParam.graphs.map(function(x,index){
                                            if(x.fillColorsField){
                                                delete x.fillColorsField;
                                                delete x.lineColorField;
                                            }
                                            if(objParam.colorSelect){
                                                x.fillColors = objParam.colorSelect;
                                            }
                                            else if(objParam.colors){
                                                x.fillColors = objParam.colors[index];
                                            }
                                            else{
                                                x.fillColors = globalRandomColor();
                                            }
                                        });            
                                    }
                                } 
        
                                objParam.graphs.map(function(x){
                                    //ONLY SHOW LEGEND WHERE THE GRAPH IS VISIBLE
                                    x.visibleInLegend = false;
                                    for (var key in v.enabledGraphsTitle) {
                                        if(x.title == v.enabledGraphsTitle[key]){
                                            x.visibleInLegend = true;
                                            break;
                                        }
                                    }
                                    x.bullet="round";
                                    x.bulletSize=10;
                                    x.fillAlphas=0.5;
                                });
        
                                var chart = AmCharts.makeChart(objParam.divId, objParam);

                                //INTERVALS
                                if(objParam.interval && parseInt(objParam.interval) > 0){
                                    var index = globalIntervalIds.indexOf(objParam.id);
                                    if (index > -1) {
                                        globalIntervalIds.splice(index, 1);
                                        clearInterval(globalIntervalTimers[objParam.id]);
                                    }                                        
                                    globalIntervalGenerator("chart",objParam.id,objParam.interval);
                               }                                
                                
                                   chart.animateAgain();
                                   chart.addListener("clickGraphItem", function (event) {
                                    var category = (event.item.dataContext.label);
                                    if(objParam.chartclick){
                                        if(objParam.chartclick.method){
                                            objParam.chartclick.extraquery_link_id[0].target_value = category;
        
                                            if(!globalModalFullscreen){
                                                if($("#"+objParam.divId).attr("data-tablefilter")){
                                                    objParam.chartclick.tablefilter = $("#"+objParam.divId).attr("data-tablefilter");
                                                }
                                            }
                                            else{
                                                if($("#"+objParam.sourceId).attr("data-tablefilter")){
                                                    objParam.chartclick.tablefilter = $("#"+objParam.sourceId).attr("data-tablefilter");
                                                }
                                            }

                                            if(objParam.querystring.graphCount > 1){
                                                objParam.chartclick.stackvalue = event.graph.valueField;
                                            }           

                                            globalLoadModal(objParam.chartclick);                   	    
                                        }
                                    }
                                });
                            }
                            else{
                                globalNoDataMessage(objParam.divId);
                            }                       		        
                        });
                    } 
                    globalExpandCollapsePortlet(objParam.id);
                }
                catch(e){
                    globalFailedMessage(objParam.divId);
                    console.log(e);
                }
            }
        });      		              
    }        
}    