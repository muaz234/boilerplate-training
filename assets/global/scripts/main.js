var Main = function () {
    return {
        init: function () {        
            //GLOBAL AJAX EVENTS
            //APPEND DEFAULT NECESSARY QUERYSTRINGS TO ALL AJAX REQUESTS
            $(document).ajaxSend(function( event, jqxhr, settings ) {
                if(settings.url.indexOf("ajax_call.php") >= 0){
                    if(event.type == "ajaxSend"){
                        if(settings.type == "GET"){
                            settings.url += '&user_id='+$("#custom_dash_user_id").val()+'&width='+$(window).width()+'&height='+$(window).height();
                        }
                        else if(settings.type == "POST"){
                            settings.data += '&user_id='+$("#custom_dash_user_id").val()+'&width='+$(window).width()+'&height='+$(window).height();
                        }
                    }    
                }                
            });
            //AJAX STARTED
            $(document).ajaxStart(function( event, jqxhr, settings ) {
                $(".custom-ajax-activity").removeClass("hidden");
                $(".logo-default").addClass("faa-flash animated");
            });
            //AJAX STOPPED
            $(document).ajaxStop(function( event, jqxhr, settings ) {
                $(".custom-ajax-activity").addClass("hidden");
                $(".logo-default").removeClass("faa-flash animated");
            });

            //POLL OVERALL SPEED
            var refreshPoll = 15000; // every 10 minutes in milliseconds
            if($(".custom-speedicon")){
                setTimeout(function(){ 
                    globalSpeedTest(5000,function(response){
                        $(".custom-speedicon-init").addClass("hidden");
                        $(".custom-title-notification").fadeIn(700);
                    });
                },1000);
                
                setInterval(function(){
                    globalSpeedTest(5000,function(response){
                        //DO NOTHING
                    });
                },refreshPoll);
            }
            
            //INACTIVITY WARNING & LOGOUT - PARENT
            if($("#custom_dash_autologout_enable").val() == 1){
                //console.log(window.opener);
                if(!window.opener){ //COUNTDOWN FOR PARENT ONLY
                    var logoutCountdown = 0;

                    setInterval(function(){
                        $("#custom_dash_user_time").val(parseInt($("#custom_dash_user_time").val())+1);
    
                        logoutCountdown = parseInt($("#custom_dash_user_time").attr("data-logout")) - parseInt($("#custom_dash_user_time").val());
                        $("#custom_logoutwarn_timer").text(logoutCountdown);
    
                        if(parseInt($("#custom_dash_user_time").val()) == parseInt($("#custom_dash_user_time").attr("data-notification"))){
                            if($("#custom_network_status_flag").val() == 1){
                                $(".preloader-overlay-logoutwarn").fadeIn(500);    
                                $("#custom_dash_user_time_warnflag").val(1);
                            }
                        }                
                        else if(parseInt($("#custom_dash_user_time").val()) == parseInt($("#custom_dash_user_time").attr("data-logout"))){
                            if($("#custom_network_status_flag").val() == 1){
                                globalCloseChildWindows('../login.php?status=logout&lang='+$("#custom_dash_language").val());
                            }
                        }                
                    },1000);
                }
                /*
                $(document).mousemove(function(e) {
                    resetTimeoutTimer();
                });
                */
                $(document).keypress(function(e) {
                    resetTimeoutTimer();
                });
            }
            
            function resetTimeoutTimer(){
                if(document.getElementById("custom_dash_page_child")){
                    if(window.opener.document.getElementById("custom_dash_user_time")){
                        window.opener.document.getElementById("custom_dash_user_time").value = 0;
                        if(window.opener.document.getElementById("custom_dash_user_time_warnflag").value == 1){
                            window.opener.document.getElementById("custom_dash_user_time_warnflag").value = 1;
                            var x = window.opener.document.getElementsByClassName("preloader-overlay-logoutwarn");
                            var i;
                            for (i = 0; i < x.length; i++) {
                                x[i].style.display = "none";
                            }                        
                        }    
                    }
                }  
                else{
                    $("#custom_dash_user_time").val(0);
                    if($("#custom_dash_user_time_warnflag").val() == 1){
                        $(".preloader-overlay-logoutwarn").fadeOut(500);   
                        $("#custom_dash_user_time_warnflag").val(0); 
                    }
                }  
            }

            //CLOSE ALL CHILD WINDOWS
            $(".custom-child-link").on("click",function(){
                var url = $(this).attr("data-url");
                var name = $(this).attr("data-name");
                var type = $(this).attr("data-type");
                
                if(name == "_blank"){
                    globalOpenedWindows[globalOpenedWindows.length] = window.open(url,name);
                }
                else{
                    globalCloseChildWindows(url);
                }
            });
            //INACTIVITY WARNING & LOGOUT - PARENT

            //PRELOADER GLOW
            var glower = $('.preloader-error');
            window.setInterval(function() {  
                glower.toggleClass('active');
            }, 1000);            
            
            //CUSTOM CLICK
            $(document).on( "click", ".custom-event-click", function(e) {
            //$(".custom-event-click").click(function(e){
                var data = JSON.parse($(this).attr("data-param"));
                
                if(data.target == "modal"){
                    if(data.type == "apikey_modal"){
                        var params = {
                            method: "get_apikey",
                            query: "latest",
                            calltype: "main"
                        };                                
                        $.get("backend/ajax_call.php?"+$.param(params))
                        .done(function( apikey ) {       
                            var JSONdata = JSON.parse(apikey);               
                            $("#apikey_value").text(JSONdata.apikey);
                            globalLoadModal(data);
                        });                            
                    }
                    else{
                        globalLoadModal(data);
                    }
                }
                else if(data.target == "language"){
                    swal({
                        title: $("#custom_swal_areyousure").val(),
                        text: $("#custom_swal_reloadtext").val(),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: $("#custom_swal_proceed").val(),
                        cancelButtonText: $("#custom_swal_cancel").val(),
                    }).then((result) => {
                        if (result.value) {
                            globalReloadPage();

                            var params = {
                                method: data.method,
                                calltype: data.calltype,
                                language: data.language,
                            };                                
                            $.get("backend/ajax_call.php?"+$.param(params))
                            .done(function( dataLang ) {                      
                                location.reload();
                            });                      
                        }
                    });                    
                }
                else if(data.target == "theme"){
                   var currentLink = $("#custom_global_theme").attr("href");
                   var arrCurrentFile = currentLink.split('/');
                   var currentFile = arrCurrentFile[arrCurrentFile.length - 1];

                   if(currentFile != data.theme){
                       $(".custom-theme-dropdown-list").css("color","black");
                       $(".custom-theme-dropdown-list i").removeClass("fa-check-circle").addClass("fa-nocheck");

                       $(this).find(".custom-theme-dropdown-list").css("color","maroon");
                       $(this).find(".custom-theme-dropdown-list i").removeClass("fa-nocheck").addClass("fa-check-circle");

                       //IF SELECTED THEME = CURRENT CUSTOMER, ADD SPECIAL STUFF
                       var customerTheme = globalCustomerName.toLowerCase()+".css";
                       if(data.theme == customerTheme){
                           if($(".custom-customer-caption-img").length > 0) {
                                $(".custom-customer-caption-img").each(function() {
                                    $(this).removeClass("hidden");
                                    $(this).prev(".caption-icon").addClass("hidden");
                                });
                            }                           
                       }
                       else{
                           if($(".custom-customer-caption-img").length > 0) {
                                $(".custom-customer-caption-img").each(function() {
                                    $(this).addClass("hidden");
                                    $(this).prev(".caption-icon").removeClass("hidden");
                                });
                            }                           
                       }

                       var hrefLink = currentLink.replace(currentFile,data.theme);
                       $("#custom_global_theme").attr("href",hrefLink);

                        var params = {
                            method: data.method,
                            calltype: data.calltype,
                            theme: data.theme
                        };
                                    
                        $.get("backend/ajax_call.php?"+$.param(params))
                        .done(function( data ) {
                        });
                   }
                }
                else if(data.target == "privacy_policy"){
                    $(data.modal_id+" .custom-modal-title i").removeClass().addClass(data.title_icon);
                    $(data.modal_id+" .custom-modal-title-text").html(data.title_text);            
                    $(data.click_id).click();
                }    
            });
            
            //TOGGLE FILTER VISIBILE
            $(".toggle-filter-visible").on("click",function(e){
                var mainPortlet = $(this).closest(".portlet"); //closest() only traverses UP!
                var filterArea = mainPortlet.find(".custom-filter-area");

                if($(this).hasClass("btn-default")){
                    $(this).removeClass("btn-default").addClass("btn-info");
                    filterArea.animate({
                        height: [ "toggle", "swing" ]
                    }, 500);                                        
                }
                else{
                    $(this).removeClass("btn-info").addClass("btn-default");                    
                    filterArea.animate({
                        height: [ "toggle", "swing" ]
                    }, 500);                                        
                }
            });
                        
            //SELECT POSTLOAD                    
            $('.custom-postload-options').on('show.bs.select', function (e) {
                var selectID = "#"+this.id;
                    
                if($(selectID + "> option").length == 0 || ($(selectID + "> option").length == 1 && $(selectID).hasClass("custom-single-select"))){
                    //ADD THE LOADING ICON
                    $(selectID).parent().find(".custom-get-filter-data").removeClass("hidden");
                    
                    var data = JSON.parse($(this).attr("data-param"));                        
                    var params = {
                        method: data.method,
                        calltype: data.calltype,
                        query: data.query
                    };
                    
                    if(data.select_icon){
                        var iconPath = globalCustomImgPath+data.select_icon_folder;
                        var iconExt = data.select_icon_ext;
                    }
                    if(data.select_fa){
                        var iconFa = "<i class='"+data.select_fa_type+"'></i>";
                    }
                    
                    $.get("backend/ajax_call.php?"+$.param(params))
                    .done(function( data ) {
                        var attrData = JSON.parse(data);
                        for	(var i = 0; i < attrData.length; i++) {
                            var selectIcon = '';
                            if(iconPath){
                                selectIcon = '<img class=\'custom-selectpicker-icon\' src='+iconPath+attrData[i].icon+iconExt+'>';
                            }
                            else if(iconFa){
                                selectIcon = iconFa;
                            }
                            $(selectID).append('<option data-content="<span>'+selectIcon+' '+attrData[i].view+'</span>" value="'+attrData[i].value+'">'+attrData[i].view+'</option>');
                        }
                        $(selectID).selectpicker('refresh');
                        $(selectID).parent().find(".custom-get-filter-data").addClass("hidden");
                    });
                }
            });
            
            $(".custom-postload-options").on("changed.bs.select",function(e){
                var selectionVal = $(this).val();
                if(selectionVal){
                    $(this).parent().find(".custom-postload-reset").addClass("btn-warning");    
                }
                else{
                    $(this).parent().find(".custom-postload-reset").removeClass("btn-warning");
                }

                var data = JSON.parse($(this).attr("data-param"));
                
                if(data.dependencies_id){
                    $(this).attr('disabled', 'disabled'); //IF DON'T DISABLE AND CLICK FAST, DEPENDENCIES WILL DUPLICATE
                    var addqueryField = data.query.val;
                    populateSelect(selectionVal,addqueryField,data.dependencies_id,this); //USE THIS METHOD FOR ASYNC IN FOR LOOP - FOR NOW                    
                }
            });
            
            function populateSelect(selectionVal,addqueryField,arrDependenciesId,parentObj){
                arrDependenciesId.forEach(function(item, i) { //USE THIS METHOD FOR ASYNC IN FOR LOOP - FOR NOW                    
                    var selectID = "#"+item;
                    var dataDependency = JSON.parse($(selectID).attr("data-param"));
                    var dependencySelection = $(selectID).val();
                    $(selectID).empty();
                    $(selectID).parent().find(".custom-get-filter-data").removeClass("hidden");
                    dataDependency.query.addqueryField = addqueryField;
                    dataDependency.query.addqueryVal = selectionVal;
                    
                    var params = {
                        method: dataDependency.method,
                        calltype: dataDependency.calltype,
                        query: dataDependency.query
                    };

                    if(dataDependency.select_icon){
                        var iconPath = globalCustomImgPath+data.select_icon_folder;
                        var iconExt = dataDependency.select_icon_ext;
                    }

                    $.get("backend/ajax_call.php?"+$.param(params))
                    .done(function( data ) {
                        var attrData = JSON.parse(data);
                        for	(var i = 0; i < attrData.length; i++) {
                            var selectIcon = '';
                            if(iconPath){
                                selectIcon = '<img class=\'custom-selectpicker-icon\' src='+iconPath+attrData[i].icon+iconExt+'>';
                            }
                            $(selectID).append('<option data-content="<span>'+selectIcon+' '+attrData[i].view+'</span>" value="'+attrData[i].value+'">'+attrData[i].view+'</option>');
                        }
                        $(parentObj).removeAttr('disabled'); //ENABLE BACK BEFORE REFRESH
                        $(selectID).selectpicker('refresh');
                        $(selectID).selectpicker('val', dependencySelection);
                        $(selectID).parent().find(".custom-get-filter-data").addClass("hidden");
                        if(!$(selectID).val()){
                            $(selectID).parent().find(".custom-postload-reset").removeClass("btn-warning");    
                        }
                    });
                });                
            } 

            //FILTERS FORM SUBMIT
            $(".custom-form-filters-submit").on("click",function(){
                var portletId = $(this).closest(".custom-main-portlet").attr("id");
                var portletBodyId = portletId+"_body";
                var portletChartClick = JSON.parse($("#"+portletBodyId).attr("data-chartclick"));
                
                var queryString = JSON.parse($("#"+portletBodyId).attr("data-querystring"));
                var tableFilter = {};
                var filterText = "";
                
                var filterTextDiv = $("#"+portletId).find(".custom-filtertext-container");

                //BEGIN NEW CHART & DATATABLE METHOD
                queryString.drilldown = true;
                queryString.drilldown_data = {};
                queryString.drilldown_data.filters = {};

                tableFilter.drilldown = true;
                tableFilter.drilldown_data = {};
                tableFilter.drilldown_data.filters = {};
                //END NEW CHART & DATATABLE  METHOD
                                
                //GET ALL CHECKED CHECKBOXES
                var checkBoxList = $("#"+portletId+" .form .md-radiobtn:checked");
                if(checkBoxList){
                    for(var i = 0; i < checkBoxList.length; i++){
                        if($(checkBoxList[i]).attr("data-query").toLowerCase() == "group by"){
                            //queryString.group_by = $(checkBoxList[i]).attr("data-field");
                            portletChartClick.extraquery_link_id[0].target_query = $(checkBoxList[i]).attr("data-field");

                            //BEGIN NEW CHART & DATATABLE METHOD
                            queryString.drilldown_data.group_by = $(checkBoxList[i]).attr("data-field");
                            //END NEW CHART & DATATABLE METHOD
                        }
                        else if($(checkBoxList[i]).attr("data-query").toLowerCase() == "order by"){
                            //queryString.order_by = $(checkBoxList[i]).attr("data-field");

                            //BEGIN NEW CHART & DATATABLE METHOD
                            queryString.drilldown_data.order_by = $(checkBoxList[i]).attr("data-field");
                            //END NEW CHART & DATATABLE METHOD
                        }
                        else if($(checkBoxList[i]).attr("data-query").toLowerCase() == "order type"){
                            //queryString.order_type = $(checkBoxList[i]).attr("data-field");

                            //BEGIN NEW CHART & DATATABLE METHOD
                            queryString.drilldown_data.order_type = $(checkBoxList[i]).attr("data-field");
                            //END NEW CHART & DATATABLE METHOD
                        }
                    }                    
                }
                
                //GET ALL SELECTPICKER VALUES
                var selectList = $("#"+portletId+" .form .selectpicker");
                if(selectList){
                    for(var j = 0; j < selectList.length; j++){
                        var selectValue = "";
                        if($(selectList[j]).val()){
                            selectValue = $(selectList[j]).val();
                        }                                
                        var value = selectValue;
                        //var field = $(selectList[j]).attr("data-field");
                        //queryString[field] = value;
                        //tableFilter[field] = value;

                        //BEGIN NEW CHART & DATATABLE METHOD
                        var paramObj = JSON.parse($(selectList[j]).attr("data-param"));
                        queryString.drilldown_data.filters[paramObj.query.val] = value;
                        tableFilter.drilldown_data.filters[paramObj.query.val] = value;
                        //END NEW CHART & DATATABLE METHOD
                        
                        if(value){
                            var name = $(selectList[j]).attr("data-name");
                            filterText += "<div class='custom-filtertext-label'><label class='label label-default label-sm'> "+name+":</label> "+JSON.stringify(value).toUpperCase()+"</div>";
                        }
                    }    
                }                
                $("#"+portletBodyId).attr("data-querystring",JSON.stringify(queryString));
                $("#"+portletBodyId).attr("data-chartclick",JSON.stringify(portletChartClick));
                $("#"+portletBodyId).attr("data-tablefilter",JSON.stringify(tableFilter));
                
                if(filterText){
                    filterTextDiv.fadeIn(500);
                    filterText = "<span class='custom-filtertext-title'><i class='fa fa-search'></i> "+$("#chart_filter_result").val()+"</span>"+filterText;
                    filterTextDiv.html(filterText);
                }
                else{
                    filterTextDiv.fadeOut(500);
                }
                                
                globalChangeChart(portletId,false,"","");
            });
            
            $(".custom-chart-refresh").on("click",function(e){
                var portletId = $(this).closest(".custom-main-portlet").attr("id");
                globalChangeChart(portletId,false,"","refresh");
            });                
            
            $(".custom-postload-reset").on("click",function(e){
                var currentSelect = $(this).parent().find("select");
                currentSelect.empty();
                currentSelect.selectpicker('refresh');
                var currentSelectParam = JSON.parse(currentSelect.attr("data-param"));
                if(currentSelectParam.dependencies_id){
                    for(var i = 0; i < currentSelectParam.dependencies_id.length; i++){
                        $("#"+currentSelectParam.dependencies_id[i]).empty();
                        $("#"+currentSelectParam.dependencies_id[i]).selectpicker('refresh');
                        $("#"+currentSelectParam.dependencies_id[i]).parent().find(".custom-postload-reset").removeClass("btn-warning");
                    }
                }
                $(this).parent().find(".custom-postload-reset").removeClass("btn-warning");
            }); 

            $(".custom-postload-reset-all").on("click",function(e){
                var currentForm = $(this).closest(".form");
                //RESET SELECT
                var currentSelect = currentForm.find("select");
                currentSelect.empty();
                currentSelect.selectpicker('refresh');    
                currentForm.find(".custom-postload-reset").removeClass("btn-warning");
                
                //RESET RADIO BUTTONS
                var currentCheckbox = currentForm.find(".custom-radio-default");
                currentCheckbox.click();
                
                //CLICK PROCEED BUTTON
                var currentProceedButton = currentForm.find(".custom-form-filters-submit");
                currentProceedButton.click();
            }); 
            
            //AUTOCOMPLETE
            var params = {
                method: "get_autocomplete_globalsearch",
                total: parseInt($("#custom_dash_globalsearch_total").val()),
                calltype: $('#autocomplete_globalsearchbox').attr("data-calltype")
            };
            
            $('#autocomplete_globalsearchbox').autocomplete({
                deferRequestBy: 500,
                serviceUrl: 'backend/ajax_call.php?'+$.param(params),
                paramName: 'searchString',
                triggerSelectOnValidInput: false,
                transformResult: function(data) {
                    var jsonDATA = jQuery.parseJSON(data);
                    return {
                        suggestions: $.map(jsonDATA, function(dataItem) {
                            return { value: dataItem.value, data: dataItem.data };
                        })
                    };                    
                },
                onSelect: function (suggestion) {
                    callbacks.autocompleteEvent(suggestion);
                }            
            }); 
            
            //FEEDBACK
            $(".custom-btn-feedback-send").on("click",function(){
                if(quillFeedback.getLength() > 1){
                    
                    var oriTitle = $("#custom_feedback_editor_title").html();                    
                    $("#custom_feedback_editor_title").html($("#feedback_progressing").val());

                    $.post( "backend/ajax_call.php", { 
                        method: "get_send_feedback",
                        calltype: "feedback",
                        subject: $("#feedback_subject").val(),
                        feedback: quillFeedback.root.innerHTML
                    })
                    .done(function( data ) {
                        if(data) {
                            try {
                                JSONdata = JSON.parse(data);
                                if(JSONdata.status == "pass"){
                                    $("#custom_feedback_editor_title").html(oriTitle);
                                    $(".custom-btn-feedback-cancel").click();
                                    toastr.success($("#feedback_success").val(),$("#toastr_generic_success_title").val()); 
                                }
                                else{
                                    $("#custom_feedback_editor_title").html(oriTitle);
                                    swal($("#swal_generic_error_title").val(), $("#swal_generic_error_desc").val(), "error");
                                    console.log(JSONdata.code);
                                }
                            } 
                            catch(e) {
                                $("#custom_feedback_editor_title").html(oriTitle);
                                swal($("#swal_generic_error_title").val(), $("#swal_generic_error_desc").val(), "error");
                                console.log(e);
                            }
                        }
                    });                    
                }
            });
            
            $(".custom-btn-feedback-clear").on("click",function(){
                if(quillFeedback.getLength() > 1){
                    quillFeedback.setText('');
                }
            });

            //APIKEY
            $(".custom-btn-apikey-generate").on("click",function(){
                var oriTitle = $("#custom_apikey_title").html();                    
                $("#custom_apikey_title").html($("#apikey_progressing").val());

                var params = {
                    method: "get_apikey",
                    query: "reset_apikey",
                    calltype: "main"
                };                                
                $.get("backend/ajax_call.php?"+$.param(params))
                .done(function( apikey ) {       
                    var JSONdata = JSON.parse(apikey);               
                    $("#apikey_value").text(JSONdata.apikey);

                    $("#custom_apikey_title").html(oriTitle);
                    toastr.success($("#toastr_generic_success_desc").val(),$("#toastr_generic_success_title").val());
                });                 
            }); 
            
            //TOGGLE INTERVAL PAUSE RESUME
            $(".custom-caption-interval-pending").on("click", function(){
                if($(this).hasClass("btn-default")){
                    $(this).removeClass("btn-default").addClass("btn-danger");
                }
                else{
                    $(this).removeClass("btn-danger").addClass("btn-default");
                }
            });  
            
            $(".custom-modal-expand-button").on("click", function(){
                var modalId = $(this).closest(".modal").attr("id");
                if(!$("#"+modalId).attr("expanded") || $("#"+modalId).attr("expanded") == "false"){
                    $("#"+modalId).attr("original-width",$("#"+modalId).css("width"));
                    $("#"+modalId).attr("expanded","true");
                    $("#"+modalId).css("width",globalModalExpandWidth);
                }
                else{
                    $("#"+modalId).css("width",$("#"+modalId).attr("original-width"));
                    $("#"+modalId).attr("expanded","false");
                }
                globalModalFullCenter("#"+modalId);

                //REDRAW DATATABLES ON MODAL RESIZE IF OBJECT EXISTS
                if(Object.keys(globalDTObjectReference).length > 0){
                    setTimeout(function(){
                        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
                    }, 1000);   
                }                    
            });

            $(".custom-modal-balance-button").on("click", function(){
                var modalId = $(this).closest(".modal").attr("id");
                globalModalFullCenter("#"+modalId);
            });            
        },
    };
}();