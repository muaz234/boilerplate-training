<?php

/* Author: Swapnil Joshi */
/* mail:   swapnil.gnu@gmail.com */

/******************************************************************************/
/*                                                                            */
/* The MIT License Copyright                                                  */ 						
/*                                                                            */
/* Copyright (c) 2012 Swapnil M. Joshi                                        */

/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/*                                                                            */
/******************************************************************************/




?>
<?php

	include_once('lib.php');
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Visual Query Designer</title>
        <!-- FAVICON -->
        <link rel="shortcut icon" href="<?php echo $data_layout['path_favicon']; ?>" />

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="../../common/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../../common/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../../common/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../common/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="../../common/assets/global/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- FOR CUSTOM DROPDOWN -->
        <link href="../../common/assets/global/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../common/assets/global/plugins/select2/css/select2.css" rel="stylesheet" type="text/css"/>
        <link href="../../common/assets/global/plugins/select2/css/select2-bootstrap.min.css rel="stylesheet" type="text/css""/>
        <!-- END CUSTOM DROPDOWN -->
        <!-- FOR MODALS -->
        <link href="../../common/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="../../common/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <!-- END MODALS --> 
        <!-- BEGIN DATATABLES -->
        <link href="../../common/assets/global/plugins/datatables/DataTables-1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../common/assets/global/plugins/datatables/Buttons-1.4.2/css/buttons.bootstrap.css" rel="stylesheet" type="text/css"/>
        <!-- END DATATABLES -->    
        <!-- BEGIN QUILL -->
        <link href="../../common/assets/global/plugins/quill/quill.snow.css" rel="stylesheet" type="text/css"/>
        <!-- END QUILL -->        
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../../common/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <!--<link href="../common/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>-->
        <!-- END THEME GLOBAL STYLES -->       
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="../../common/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="../../common/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <!-- END THEME LAYOUT STYLES -->
        <!-- BEGIN CUSTOM LAYOUT -->
        <link href="../../common/assets/global/plugins/amcharts/amcharts/plugins/export/export.css" rel="stylesheet" type="text/css"/>
        <link href="../../common/assets/global/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../common/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,400,700" rel="stylesheet"/>
        <link href="../../common/assets/global/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />   
        <link href="../../common/assets/global/plugins/mapping/leaflet-120/leaflet.css" rel="stylesheet" type="text/css"/>
        <link href="../../common/assets/global/plugins/mapping/awesome-markers/leaflet_awesome_markers.css" rel="stylesheet" type="text/css"/>
        <link href="../../common/assets/global/plugins/mapping/css/main.css" rel="stylesheet" type="text/css"/>        
        <link href="../../common/assets/global/plugins/mapping/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="../../common/assets/global/plugins/mapping/locate/css/L.Control.Locate.min.css" rel="stylesheet" type="text/css"/>        
        <link href="../../common/assets/global/plugins/mapping/polyline-measure/Leaflet.PolylineMeasure.css" rel="stylesheet" type="text/css"/>      
        <link href="../../common/assets/global/css/general.css" rel="stylesheet" type="text/css" />
        <link href="../../common/assets/global/css/generator.css" rel="stylesheet" type="text/css"/>    
        <link href="../../common/assets/global/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../../common/assets/global/themes/<?php echo $data_layout['user_theme'];?>" rel="stylesheet" type="text/css" />        
        <!-- END CUSTOM LAYOUT -->
        <!-- START CUSTOM LAYOUT -->
        <link href="../assets/custom/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/custom/css/custom_pivot.css" rel="stylesheet" type="text/css"/>
        <!-- END CUSTOM LAYOUT -->   
        <link rel="stylesheet" type="text/css" href="css/design.css" />
   	    <link rel="stylesheet" type="text/css" href="css/tbl_list_overlay.css" />

<!--
    <script src="../../common/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="../../common/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

	<script type="application/x-javascript" src="js/jquery.min.js" > </script>
	<script type="application/x-javascript" src="js/jquery-ui.js" > </script>
-->    
<!--
	<script type="application/x-javascript" src="js/jquery.cookie.js" > </script>

	<script type="application/x-javascript" src="js/underscore-min.js"></script>
	<script type="application/x-javascript" src="js/backbone-min.js"></script>       
	<script src="js/backbone-localstorage.js"></script>
	<script type="application/x-javascript" src="js/jsplumb.js"> </script>

	<script type="text/javascript" src="coffee/core.js"> </script>
	<script type="text/javascript" src="coffee/core.panes.js"> </script>
	<script type="text/javascript" src="coffee/design.js"> </script>
  <script type="application/x-javascript" src="js/tbl-selection.js"></script>
-->
	<!--<script type="text/javascript" src="coffee/demo.js"> </script>-->
	
<!--	
	<script type="application/x-javascript">

        window.swapnil = {};
        $(function(){
				if(localStorage) localStorage.clear();
				$.cookie('schema','');
				
			    $('.btn-refresh').click(function () {
                    $('.output > pre').html(QueryBuilder.GenerateSQL()); 
                });
                
                $('.bool.expr').draggable({
                        helper:'clone'
                });
						App.setAppVisibility();
                        bindAddTableEvts();
                $("#table-selection").draggable();
		})
	</script>
-->    
</head>

<body>

<div id="content">
			
    <div id="right-pane" class="design-mode">
		<div id="tool-bar" >
        <!--
				<span id="project-logo" style="float: right;">
				<span style="" class="wb">  Web-based </span>
				<span style="" class="vqd"> Visual Query Designer </span>
				</span>
        <span style="float: left;">
				Schema : 
				<select name="schema" >
					<option value="" > -- Select schema -- </option>

					<?php

						$query= "SHOW SCHEMAS";
						$result= mysqli_query($dbc,$query);
						while($row =  mysqli_fetch_array($result)){
							$DBName=$row["Database"]
					?>
					<option value="<?php echo $DBName; ?>"><?php echo $DBName ?></option>
					<?php } ?>
				</select>
				</span>
        -->        
				
				<span id="btn-add-table" class="tool-bar-btn"> Add Table / View
				</span>
        </div>
		<div class="pane output-mode" id="sql-out">
            <div class="output">
				<!-- SQL output -->
				<pre id="sql-text-op">
					Select a schema and then add tables/views.
				</pre>
			</div>
		</div>
		
		<div id="design-pane" class="pane">
            <!--design pane-->
            
        </div>
		<fieldset id="pane-select" class="pane inline-pane">
			<legend> SELECTED FIELDS </legend>
		</fieldset>
		
		<div class="inline-pane">    
			<fieldset id="pane-join" class="pane">
					<legend>JOIN</legend>
			</fieldset>
		
			<fieldset id="pane-order-by" class="pane">
				<legend>  ORDER BY </legend>
			</fieldset>
		</div>
		<fieldset id="pane-where" class="pane inline-pane">
        <legend> WHERE </legend>
		<textarea id="" cols="30" rows="5"></textarea>
		</fieldset>
    </div>

	<div id="table-selection">
		<div id="">
			Add tables
		</div>
		<div id="table-list">
		
		</div>	
			<div id="table-btns">
				<span class="butn ok">
					GO
				</span>
				<span class="butn cancel">
					Cancel
				</span>
				
			</div>
		</div>
	</div>

</div>

<script id="table-label-template" type="text/template" >
<div class="close " title="">
    X
</div>
    <%= TableName %>

 </script>

 <script id="table-field-template" type="text/template" >
		
		<%
		strSign = '';	
		strTitle = '';
        switch(Sort){
            case 'ASC':
                strSign = 'A';
				strTitle = 'Asc';
                break;
            case 'DESC':
                
				strSign = 'D';
				strTitle = 'Desc';
                break;
            default:
                
				strSign = '+';
				strTitle = 'Click to add sort order';
        }
        %>
		
 <div class="cell-dragger" title="">
 <div class="cell-edit">
 
      <span class=" <%= Sort=='UNSORTED' ? 'hoverable' : '' %> orderby " title=" <%= strTitle %> " >
			<%= strSign %>
      </span>

 </div>
 
 <input type="checkbox" name=""
 <%= Selected ? 'checked=checked':''  %>  />
 <% var cellClass= (IsPrimaryKey) ? 'primary-key':''  %>
                    <span class="field-name <%= cellClass %>" style="" >
                        <%= ColumnName %>
                        <%= (Selected)? '': '' %>
                    </span>
 </div>
     
 </script>
 
 <script id="paneLi-select-template" type="text/template" >
    <div class="<%= Selected ? 'line-item': 'hidden-item' %> ">

        
        <div class="icon delete"></div>
        <div class="property-editor">
            
            <div class="editable ">
								<span class="label">
								AS
								</span>
								<input type="text" class="alias" value="<%= Alias %>" />
            </div>
						
						<div class="field-name">
                <%=  TableName  %>.<%= ColumnName %>
            </div>
            
        </div>
    </div>

 </script>
 
<script id="paneLI-join-template" type="text/template" >
            <div class="line-item">                              
                
                <div class="property-editor">
                    <div class="field-name">
                          <%=  LeftTable %><%=  (LeftField=='') ? '' : '.'+ LeftField  %>
                    </div>
                    <div class="" style="padding-right:10px;">
                    <%
                        selInner =  (Type == 'INNER') ? " selected='selected' " : "";
                        selLeftOut =  (Type == 'LEFT_OUTER') ? "selected='selected'" : "";

                        selRightOut =   (Type == "RIGHT_OUTER") ? "selected='selected'" : ""
                        /*

                        */
                    %>
                        <% if(Type =='CROSS_JOIN') { %>
                            Cross Join
                        <% }else{ %>
                        <select name="">
                            <option <%= selInner %> value=<%= 'INNER_JOIN' %>   > INNER JOIN </option>
                            <option <%= selLeftOut %>  value=<%= 'LEFT_OUTER' %>  > LEFT JOIN</option>
                            <option <%= selRightOut %> value=<%= 'RIGHT_OUTER' %>  > RIGHT JOIN </option>
                        </select>
                        
                        <% } %>
                    </div>
                    <div class="field-name">
												<%=  RightTable %><%=  (RightField=='') ? '' : '.'+ RightField  %>

                    </div>
                </div>
    
            </div>

</script>

<script id="paneLI-orderby-template" type="text/template" >
<div class="<%= (Sort !== 'UNSORTED') ? 'line-item': 'hidden-item' %> ">                
    
    <div class="icon delete"></div>
    <div class="property-editor">
        <div class="field-name">
            <%=  TableName  %>.<%= ColumnName %>
        </div>
        <div class="" style="padding-right:10px;float: right;">
            <select name="">
            <% var selAsc = (Sort== 'ASC') ? "selected='selected'" : ""  %>
            <% var selDesc = (Sort== 'DESC') ? "selected='selected'" : ""  %>
                <option <%= selAsc %> value="ASC" > Asc </option>
                <option <%= selDesc %> value="DESC"  > Desc </option>
            </select>
        </div>

    </div>
</div>
    
</script>

<script id="sql-pane-template" type="text/template" >
</script>

<script id="menu-item-template" type="text/template" >
    <div class="menu-item">
        <a href="<%= link %>">
            <%= label %>
        </a>
    </div> 
</script>

<!--[if lt IE 9]>
        <script src="assets/global/plugins/respond.min.js"></script>
        <script src="assets/global/plugins/excanvas.min.js"></script> 
        <script src="assets/global/plugins/ie8.fix.min.js"></script> 
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="../../common/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript" /></script>
        <script src="../../common/assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../../common/assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../../common/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../../common/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>  
        <script src="../../common/assets/global/plugins/sweetalert/sweetalert2.min.js" type="text/javascript"></script>   
        <script src="../../common/assets/global/plugins/lodash/lodash.full.min.js" type="text/javascript"></script>                   
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN DATATABLES PLUGINS -->
        <script src="../../common/assets/global/plugins/datatables/DataTables-1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/datatables/DataTables-1.10.16/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/datatables/Buttons-1.4.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/datatables/Buttons-1.4.2/js/buttons.bootstrap.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="../../common/assets/global/plugins/datatables/pdfmake-0.1.32/pdfmake.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/datatables/pdfmake-0.1.32/vfs_fonts.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/datatables/Buttons-1.4.2/js/buttons.html5.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/datatables/Buttons-1.4.2/js/buttons.print.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/datatables/Buttons-1.4.2/js/buttons.colVis.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/datatables/Moment/datatables.moment.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/colorchroma/chroma.min.js" type="text/javascript"></script>        
        <script src="../../common/assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js" type="text/javascript"></script>        
        <script src="../../common/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>        
        <!-- END DATATABLES PLUGINS -->    
        <!-- BEGIN FLEXMONSTER PLUGINS -->
        <!--<script src="../common/assets/global/plugins/flexmonster/flexmonster.js" type="text/javascript"></script>-->
        <!-- END FLEXMONSTER PLUGINS -->
        <!-- BEGIN ACE EDITOR PLUGINS -->
        <script src="../../common/assets/global/plugins/ace-editor/ace.js" type="text/javascript"></script>
        <!-- END ACE EDITOR PLUGINS -->
        <!-- BEGIN BOOTSTRAP SELECT -->
        <script src="../../common/assets/global/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
        <!-- END BOOTSTRAP SELECT -->                        
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="../../common/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="../../common/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <!-- START PIVOT JS -->
        <script src="../../common/assets/global/plugins/pivotjs/dist/pivot.gen.js" type="text/javascript"></script>        
        <!-- END PIVOT JS -->        
        <!-- BEGIN CUSTOM PLUGINS -->
        <script src="https://maps.google.com/maps/api/js?key=AIzaSyBNRABiKOobsfWYTOGSBYu-b4aQXuDeh-A" type="text/javascript" ></script>   
        <script src="../../common/assets/global/plugins/mapping/leaflet-120/leaflet.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/mapping/bing/leaflet-bing-layer.min.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/mapping/bing/Bing.js" type="text/javascript"></script>        
        <script src="../../common/assets/global/plugins/mapping/awesome-markers/leaflet_awesome_markers.js" type="text/javascript"></script>        
        <script src="../../common/assets/global/plugins/mapping/bouncemarker/bouncemarker.js" type="text/javascript"></script>
        <script src="../../common/assets/global/plugins/mapping/locate/js/L.Control.Locate.min.js" type="text/javascript"></script>        
        <script src="../../common/assets/global/plugins/mapping/polyline-measure/Leaflet.PolylineMeasure.js" type="text/javascript"></script>        
        <script src="../../common/assets/global/plugins/jqueryautocomplete/jquery.autocomplete.pqre.js" type="text/javascript"></script>        
        <script src="../../common/assets/global/plugins/quill/quill.min.js" type="text/javascript"></script>        
        <!-- END CUSTOM PLUGINS -->   
        <!-- BEGIN CUSTOM GLOBAL SCRIPTS -->
        <script src="../../common/assets/global/scripts/global.js" type="text/javascript"></script>
        <script src="../../common/assets/global/scripts/modaldata.js" type="text/javascript"></script>
        <script src="../../common/assets/global/scripts/chartfunctions.js" type="text/javascript"></script>
        <script src="../../common/assets/global/scripts/main.js" type="text/javascript"></script>
        <script src="../../common/assets/global/scripts/portlet.js" type="text/javascript"></script>
        <script src="../../common/assets/global/scripts/amcharts.js" type="text/javascript"></script>
        <script src="../../common/includes/geojson/countries/countryBoundary.js" type="text/javascript"></script>
        <!-- END CUSTOM GLOBAL SCRIPTS -->
        <!-- BEGIN CUSTOM SCRIPTS -->
        <script src="../assets/custom/js/callbacks.js" type="text/javascript"></script>
        <script src="../assets/custom/js/custom.js" type="text/javascript"></script>
        <script src="../assets/custom/js/mapbase.js" type="text/javascript"></script>
        <script src="../assets/custom/js/generator.js" type="text/javascript"></script>
        <!-- END CUSTOM SCRIPTS -->

        <script type="application/x-javascript" src="js/jquery.cookie.js" > </script>

<script type="application/x-javascript" src="js/underscore-min.js"></script>
<script type="application/x-javascript" src="js/backbone-min.js"></script>       
<script src="js/backbone-localstorage.js"></script>
<script type="application/x-javascript" src="js/jsplumb.js"> </script>

<script type="text/javascript" src="coffee/core.js"> </script>
<script type="text/javascript" src="coffee/core.panes.js"> </script>
<script type="text/javascript" src="coffee/design.js"> </script>
<script type="application/x-javascript" src="js/tbl-selection.js"></script>

<script type="application/x-javascript">

window.swapnil = {};
$(function(){
        if(localStorage) localStorage.clear();
        $.cookie('schema','');
        
        $('.btn-refresh').click(function () {
            $('.output > pre').html(QueryBuilder.GenerateSQL()); 
        });
        
        $('.bool.expr').draggable({
                helper:'clone'
        });
                App.setAppVisibility();
                bindAddTableEvts();
        $("#table-selection").draggable();
})
</script>
</body>
</html>
